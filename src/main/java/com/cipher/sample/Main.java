/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cipher.sample;

import com.cipher.sample.crypto.Crypto;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ghost
 */
public class Main {
    
    public static void main(String[] args){
        ObjectMapper mapper = new ObjectMapper();
        try {
            //Read raw value and its encrypted from node output
            Map<String, Object> input = mapper.readValue(new File("node/cipher-output.json"), Map.class);
            String raw = (String) input.get("raw"),
                    encrypted = (String) input.get("encrypted"),
                    secret = (String) input.get("secret");
            
            
            //print comparison
            String redecrypt = Crypto.encryptAES192(raw, secret);
            System.out.println("Raw data     : " + raw);
            System.out.println("Encrypted    : " + encrypted);
            System.out.println("Java encrypt : " + redecrypt);
            System.out.println("Java dencrypt: " + Crypto.decryptAES192(redecrypt, secret));
            
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
