/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cipher.sample.crypto;

import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author Ghost
 */
public class Crypto {
    
    private static byte[][] EVP_BytesToKey(int key_len, int iv_len, MessageDigest md, byte[] salt, byte[] data, int count) {
        byte[][] both = new byte[2][];
        byte[] key = new byte[key_len];
        int key_ix = 0;
        byte[]  iv = new byte[iv_len];
        int iv_ix = 0;
        both[0] = key;
        both[1] = iv;
        byte[] md_buf = null;
        int nkey = key_len;
        int niv = iv_len;
        int i = 0;
        if(data == null) {
            return both;
        }
        int addmd = 0;
        for(;;) {
            md.reset();
            if(addmd++ > 0) {
                md.update(md_buf);
            }
            md.update(data);
            if(null != salt) {
                md.update(salt,0,8);
            }
            md_buf = md.digest();
            for(i=1;i<count;i++) {
                md.reset();
                md.update(md_buf);
                md_buf = md.digest();
            }
            i=0;
            if(nkey > 0) {
                for(;;) {
                    if(nkey == 0) break;
                    if(i == md_buf.length) break;
                    key[key_ix++] = md_buf[i];
                    nkey--;
                    i++;
                }
            }
            if(niv > 0 && i != md_buf.length) {
                for(;;) {
                    if(niv == 0) break;
                    if(i == md_buf.length) break;
                    iv[iv_ix++] = md_buf[i];
                    niv--;
                    i++;
                }
            }
            if(nkey == 0 && niv == 0) {
                break;
            }
        }
        for(i=0;i<md_buf.length;i++) {
            md_buf[i] = 0;
        }
        return both;
    }
    
    
    
    public static String encryptAES192(String data, String password) {
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            Integer keySizeBits = 192 / Byte.SIZE; //AES with 192 bits key = 16 bytes
            Integer ivSize = cipher.getBlockSize();
            final byte[][] keyAndIV = EVP_BytesToKey(keySizeBits, ivSize, md5, null, password.getBytes(StandardCharsets.US_ASCII), 1);
            SecretKeySpec key = new SecretKeySpec(keyAndIV[0], "AES");
            IvParameterSpec iv = new IvParameterSpec(keyAndIV[1]);

            cipher.init(Cipher.ENCRYPT_MODE, key, iv);

            byte[] cipherText = cipher.update(data.getBytes());
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            buffer.write(cipherText);
            buffer.write(cipher.doFinal());

            String output = DatatypeConverter.printHexBinary(buffer.toByteArray());
            output = output.substring(0, output.length() / 2);
            return output.toLowerCase();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
    
    
    public static String decryptAES192(String encoded, String password){
        try {
            
            
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            Integer keySizeBits = 192 / Byte.SIZE; //AES with 192 bits key = 16 bytes
            Integer ivSize = cipher.getBlockSize();
            final byte[][] keyAndIV = EVP_BytesToKey(keySizeBits, ivSize, md5, null, password.getBytes(StandardCharsets.US_ASCII), 1);
            SecretKeySpec key = new SecretKeySpec(keyAndIV[0], "AES");
            IvParameterSpec iv = new IvParameterSpec(keyAndIV[1]);
            
            byte[] encodedBytes = DatatypeConverter.parseHexBinary(encoded.toUpperCase());
            cipher.init(Cipher.DECRYPT_MODE, key, iv);
            byte[] cipherDecrypt = cipher.update(encodedBytes);
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            buffer.write(cipherDecrypt);
            buffer.write(cipher.doFinal());
            
            return buffer.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
