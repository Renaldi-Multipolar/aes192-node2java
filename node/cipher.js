const crypto = require('crypto'),
    algorithm = 'aes192',
    secret = 'sayaadalahprogrammerbahagia';

function encrypt(text) {
    var cipher = crypto.createCipher(algorithm, secret)
    var crypted = cipher.update(text, 'utf8', 'hex')
    crypted += cipher.final('hex');
    return crypted;
}

function decrypt(text) {
    var decipher = crypto.createDecipher(algorithm, secret)
    var dec = decipher.update(text, 'hex', 'utf8')
    dec += decipher.final('utf8');
    return dec;
}

const raw = "whatzittooya";

const encrypted = encrypt(raw);
const decrypted = decrypt(encrypted)

console.log(`Raw: ${raw}`);
console.log(`Encrypted: ${encrypted}`);
console.log(`Decrypted: ${decrypted}`);

//Save raw and encrypted data to compare
const fs = require('fs');
fs.writeFileSync("cipher-output.json", JSON.stringify({raw, secret, encrypted}));