# Converting AES192
---
This repositories is an example implementation of AES192 encryption from NodeJS encryption to Java

Based on Stackoverflow question [https://stackoverflow.com/questions/68418038/how-to-port-encrypt-aes192-method-from-nodejs-to-java-correctly](in here)

Requirement:
..1. NodeJS

..2. JDK 8

..3. Any java IDE with maven project support

How to use:
..1. Open terminal in this root project

..2. Execute `node node/cipher.js`

..3. Open this repository in your IDE

..4. Build and execute Main class


The `Crypto` class can be reused as you seems fit.